<?php

namespace app\Factories;

/**
 * Class DataProviderFactory
 * @package app\Factories
 */
final class DataProviderFactory
{
    /**
     * @param $name
     * @param \cli\DI $DI
     * @return \app\Providers\DataProvider
     */
    public static function create($name, \cli\DI $DI)
    {
        echo 'create new instant of ' . $name . ' DataProvider ' . PHP_EOL;
        $provider_class = '\\app\\Providers\\' . $name;
        return new $provider_class($DI);
    }
}