<?php

namespace app\Factories;

/**
 * Class DatabaseFactory
 * @package app\Factories
 */
final class DatabaseFactory
{
    /**
     * @param $DI
     * @return \app\Adapters\DatabaseAdapter
     */
    public static function create($DI)
    {

        echo 'create new instant of ' . $DI->config()['database']['adapter'] . ' DatabaseAdapter ' . PHP_EOL;
        $adapter_class = '\\app\\Adapters\\' . $DI->config()['database']['adapter'];
        return new $adapter_class($DI);
    }
}