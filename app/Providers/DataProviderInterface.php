<?php

namespace app\Providers;

/**
 * Interface DataProviderInterface
 * @package app\Providers
 */
interface DataProviderInterface
{
    /**
     * @return array
     */
    public function getData();
}