<?php

namespace app\Providers;

/**
 * Class Glorf
 * @package app\Providers
 */
final class Glorf extends DataProvider
{
    /**
     * Return array contains entities: Video, Tags, VideoTags
     * @return array
     */
    public function getData()
    {
        echo 'Glorf: get data from specific source' . PHP_EOL;
        // TODO: Implement getData() method.
        return [
            'video'         =>  new \app\Entities\Video($this->DI),
            'tag'           =>  new \app\Entities\Tag($this->DI),
            'video_tags'    =>  new \app\Entities\VideoTag($this->DI)
        ];
    }
}