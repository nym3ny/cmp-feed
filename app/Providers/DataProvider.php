<?php

namespace app\Providers;

/**
 * Class DataProvider
 * @package app\Providers
 */
abstract class DataProvider implements DataProviderInterface
{
    /**
     * @var integer
     */
    protected $limit;

    /**
     * @var \cli\DI
     */
    protected $DI;

    /**
     * DataProvider constructor.
     * @param \cli\DI $DI
     */
    public function __construct(\cli\DI $DI)
    {
        $this->DI = $DI;
    }

    /**
     * @param $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }
}