<?php

namespace app\Adapters;

/**
 * Class DatabaseAdapter
 * @package app\Adapters
 */
abstract class DatabaseAdapter implements DatabaseAdapterInterface
{
    /**
     * DatabaseAdapter constructor.
     */
    public function __construct()
    {
        $this->connect();
    }
}