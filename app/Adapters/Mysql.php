<?php

namespace app\Adapters;

final class Mysql extends DatabaseAdapter
{
    /**
     * Connect to database
     */
    public function connect()
    {
        // TODO: Implement connect() method.
        echo 'start db connexion' . PHP_EOL;
    }

    /**
     * Start transaction
     */
    public function startTransaction()
    {
        // TODO: Implement startTransaction() method.
        echo 'start transaction' . PHP_EOL;
    }

    /**
     * Rollback transaction
     */
    public function rollback()
    {
        // TODO: Implement rollback() method.
        echo 'rollback' . PHP_EOL;
    }

    /**
     * Commit transaction
     */
    public function commit()
    {
        // TODO: Implement commit() method.
        echo 'commit transaction' . PHP_EOL;
    }

    /**
     * @param \app\entities\Entity $entity
     * @param $table_name
     * @param $attributes
     * @return \app\Entities\Entity
     */
    public function saveAll(\app\entities\Entity $entity, $table_name, $attributes)
    {
        // TODO: Implement saveAll() method.
        echo 'check if multiple ' . $table_name . PHP_EOL;
        echo 'insert to db all ' . $table_name . PHP_EOL;
        return $entity;
    }
}