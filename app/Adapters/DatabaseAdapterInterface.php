<?php

namespace app\Adapters;

/**
 * Interface DatabaseAdapterInterface
 * @package app\Adapters
 */
interface DatabaseAdapterInterface
{
    /**
     * Connect to database
     */
    public function connect();

    /**
     * Start transaction
     */
    public function startTransaction();

    /**
     * Rollback transaction
     */
    public function rollback();

    /**
     * Commit transaction
     */
    public function commit();

    /**
     * @param \app\entities\Entity $entity
     * @param $table_name
     * @param $attributes
     * @return \app\Entities\Entity
     */
    public function saveAll(\app\entities\Entity $entity,$table_name,$attributes);
}