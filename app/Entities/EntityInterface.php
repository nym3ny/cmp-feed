<?php

namespace app\Entities;

/**
 * Class EntityInterface
 */
interface EntityInterface extends \IteratorAggregate, \ArrayAccess
{

}