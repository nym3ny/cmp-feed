<?php

namespace app\Entities;

/**
 * Class Video
 * @package app\Entities
 */
final class Video extends Entity
{
    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $title;

    /**
     * @var integer
     */
    public $id;

}