<?php

namespace app\Entities;

/**
 * Class VideoTag
 * @package app\Entities
 */
final class VideoTag extends Entity
{
    /**
     * @var integer
     */
    public $video_id;

    /**
     * @var integer
     */
    public $tag_id;
}