<?php

namespace app\Entities;

/**
 * Class Tag
 * @package app\Entities
 */
final class Tag extends Entity
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var integer
     */
    public $id;


}