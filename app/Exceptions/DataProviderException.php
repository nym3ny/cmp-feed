<?php

namespace app\Exceptions;

/**
 * Class DataProviderException
 * @package app\Exceptions
 */
class DataProviderException extends \Exception
{

}