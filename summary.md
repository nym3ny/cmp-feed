**Installation steps**

* git clone ssh://git@bitbucket.org/nym3ny/cmp-feed.git
* cd cmp-feed/cli
* chmod +x cli

**How to rune code**

* cd cli
* cli Import Flub
* cli Import Glorf

**How to run tests**

* cd tests
* phpunit Unit/Adapters/MysqlTest.php
* phpunit Unit/Providers/FlubTest.php
* phpunit Unit/Providers/GlorfTest.php
