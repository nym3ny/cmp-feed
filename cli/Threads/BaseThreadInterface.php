<?php

namespace cli\Threads;

/**
 * Interface BaseThreadInterface
 * @package cli\Threads
 */
interface BaseThreadInterface
{
    /**
     * Main function for threads
     */
    public function run();
}