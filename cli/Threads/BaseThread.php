<?php

namespace cli\Threads;

/**
 * Class BaseThread
 * @package cli\Threads
 */
abstract class BaseThread implements BaseThreadInterface
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @var \cli\DI
     */
    protected $DI;

    /**
     * BaseThread constructor.
     * @param $data
     * @param \cli\DI $DI
     */
    public function __construct($data,\cli\DI $DI)
    {
        $this->DI = $DI;
        $this->data = $data;
    }

    /**
     * TODO: delete this function after extend pecl pthread class: Thread
     */
    public function start()
    {
        $this->run();
    }
}