<?php

namespace cli\Threads;

/**
 * Class Insert
 * @package cli\Threads
 */
final class Insert extends BaseThread
{
    /**
     * Main function for Insert thread
     */
    public function run()
    {
        try{
            $this->DI->db()->startTransaction();
            $videos = $this->DI->db()->saveAll($this->data['video'],'video',['url','title']);
            $tags = $this->DI->db()->saveAll($this->data['tag'],'tag',['name']);
            $video_tags = $this->matchVideoWithTag($videos, $tags, $this->data['video_tags']);

            $this->DI->db()->saveAll($video_tags,'video_tag',['video_id','tag_id']);
            $this->DI->db()->commit();
        }catch(\Exception $e){
            $this->DI->db()->rollback();
        }
    }

    /**
     * @param $videos
     * @param $tags
     * @param $video_tags
     * @return \app\Entities\Entity
     */
    private function matchVideoWithTag($videos, $tags, $video_tags)
    {
        return new \app\Entities\VideoTag($this->DI);
    }
}