<?php

namespace cli;

/**
 * Class DI
 * @package cli
 * Sample Dependency Injection
 */
final class DI
{
    /**
     * @var \app\Adapters\DatabaseAdapter
     */
    private $adapter;

    public function config()
    {
        return include BASE_PATH . '/config/main.php';
    }

    /**
     * @return \app\Adapters\DatabaseAdapter
     */
    public function db()
    {
        if(!$this->adapter){
            $this->adapter = \app\Factories\DatabaseFactory::create($this);
        }
        return $this->adapter;
    }

}