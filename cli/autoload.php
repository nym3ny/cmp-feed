<?php

namespace cli;

//autoload
define('BASE_PATH', realpath(dirname(__FILE__). '/../'));

spl_autoload_register(function ($class_name)
{
    $filename = BASE_PATH . '/' . str_replace('\\', '/', $class_name) . '.php';
    require_once $filename;
});