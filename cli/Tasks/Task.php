<?php

namespace cli\Tasks;

/**
 * Class Task
 */
abstract class Task implements TaskInterface
{
    /**
     * @var array
     */
    protected $params;

    /**
     * @var \cli\DI
     */
    protected $DI;

    /**
     * Task constructor.
     * @param $params
     * @param \cli\DI $DI
     */
    public function __construct($params,\cli\DI $DI)
    {
        $this->DI = $DI;
        $this->params = $params;
    }
}