<?php

namespace cli\Tasks;

/**
 * Class TaskFactory
 * @package cli\Tasks
 */
final class TaskFactory
{
    /**
     * @param $name
     * @return Task
     */
    public static function create($name,$params,$DI)
    {
        echo 'create new instance of ' . $name . ' task ' . PHP_EOL;
        $task_class = '\\cli\\Tasks\\' . $name;
        return new $task_class($params, $DI);
    }
}