<?php

namespace cli\Tasks;

/**
 * Interface TaskInterface
 * @package cli\Tasks
 */
interface TaskInterface
{
    /**
     *  Main function for tasks
     */
    public function run();
}