<?php

namespace cli\Tasks;

/**
 * Class Import
 */
final class Import extends Task
{
    /**
     * Main function for Import task
     */
    public function run()
    {
        $providerName = $this->params[2];
        $dataProvider = \app\Factories\DataProviderFactory::create($providerName, $this->DI);
        $dataProvider->setLimit(10);

        $threads = [];
        $data = $dataProvider->getData();
        $threads[] = new \cli\Threads\Insert($data,$this->DI);

        foreach($threads as $thread){
            $thread->start();
        }

    }
}