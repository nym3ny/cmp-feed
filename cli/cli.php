<?php

namespace cli;

//autoload
require 'autoload.php';

//init Dependency Injector
$DI = new DI();

//run task with args
$task = \cli\Tasks\TaskFactory::create($argv[1],$argv,$DI);
$task->run();
