<?php

/**
 * Config file
 */

return [
    'database'  =>  [
        'adapter'   =>  'mysql',
        'host'      =>  'localhost',
        'port'      =>  3306,
        'name'      =>  'localhost_db',
        'user'      =>  'username',
        'password'  =>  'password'
    ]
];